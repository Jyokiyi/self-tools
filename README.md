# self-tools

#### 介绍
项目使用的常用工具类和配置。

环境是JDK1.8

#### 目录结构
总目录结构：
<pre>
D:.
├─docker                         # Dockerfile文件存放目录
├─java
│  └─com
│      └─self
│          └─ry
│              ├─config          # 配置
│              │  └─swagger2     # API文档配置
│              ├─controller      # rest接口1
│              ├─controller2     # rest接口2
│              ├─mapper          # mapper接口类
│              ├─model           # bean,entity
│              └─utils           # 辅助工具类
│                  ├─lambda
│                  └─sm2
└─resources
    └─mybatis
        └─mapper                 # 存放XML文件
</pre>

部分类说明：
<pre>
│
├─config                          # 跳板机设定
│      MySQLSSHSettings.java      # MySQL配置读取
│      RedisSSHSettings.java      # Redis配置读取
│      SSHConnection.java         # 启动ssh连接
│
└─utils                           # 工具类
      │  BCCUtil.java             # BCC校验
      │  ByteHexUtil.java         # 字节-16进制操作
      │  CRCUtil.java             # CRC校验
      │  DateUtil.java            # 日期操作
      │  EncryptUtil.java         # 密码加密操作
      │  ExcelUtil.java           # Excel操作类
      │  IPUtil.java              # 获取IP类
      │  JwtUtil.java             # JWT--加解密操作
      │  RipeMD160Util.java       # RipeMD160加密操作
      │  RSAUtil.java             # RSA加解密操作
      │  ShiftUtil.java           # 移位和位操作
      │  StringUtils.java         # 字符串操作
      │  TimerManageUtil.java     # 定时任务 线程池操作
      │  Util.java                # 二进制 十六进制 字节数组操作
      │
      ├─lambda                    # lambda表达式，groupby多个分组
      │      Salary.java
      │      SalaryTest.java
      │      Tuple.java
      │      Tuple2.java
      │
      └─sm2                       # 国密2-加解密
              Cipher2SM2.java
              SM2EnDecryption.java
              SM2Factory.java
              SM2Result.java
              SM2Util.java
              SM3.java
              SM3Digest.java
</pre>

#### Swagger--API接口管理
采用的是`Swagger2`：
* 在应用主类中增加`@EnableSwagger2Doc`注解.

配置示例参照：https://github.com/SpringForAll/spring-boot-starter-swagger


本项目设定配置类:`com.self.ry.config.swagger2.Swagger2Config`。
使用`swagger.base-package`可以指定多个包生成API接口文档，使用`分号(;)`分隔。


项目运行后，在浏览器上输入[http://localhost:8089/swagger-ui.html](http://localhost:8089/swagger-ui.html)，在选择myselfAPI分组，就可以看到接口信息。

#### 使用说明

所有的帮助类都在是`com.self.ry.utils`包中。

#### 数据库连接管理
要在Spring中使用MyBatis，您至少需要一个SqlSessionFactory 和至少一个mapper接口。

使用`mybatis-spring-boot-starter`依赖，它自动依赖jdbc： spring-boot-starter-jdbc`的2.0+版本，默认依赖使用了HiKari数据库连接池管理。本项目即时使用默认的数据库连接池。

`mybatis-spring-boot-starter`将做以下事情：

* 自动检测现有的`Datasource`.
* 使用`SqlSessionFactoryBean`类，`Datasource`作为输入将创建并注册一个`SqlSessionFactory`实例。
* 将创建并注册`SqlSessionTemplate`的实例从`SqlSessionFactory`中获取。
* 自动扫描Mappers，将它们链接到`SqlSessionTemplate`并将它们注册到Spring上下文，以便将它们注入到bean中。

在Mapper接口类上使用`org.apache.ibatis.annotations.Mapper`或者org.springframework.stereotype.Repository`注解，

* 默认情况下，`MyBatis-Spring-Boot-Starter`将搜索标有`@Mapper`注解的映射器。
* 你可能需要指定**自定义注解**或**标记接口**以进行扫描。 若是这样，必须使用`@MapperScan`注解指定包路径（`@MapperScan(com.self.ry.mapper)`）。

配置MyBatis时查看`MybatisProperties`类对应的属性集，然后在yml文件中设置对应的属性。

#### Redis
添加依赖:

```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```
特征：

* 连接包作为跨多个Redis驱动程序/连接器（Jedis和Lettuce）的低级抽象。不支持JREDI和SRP。
* 针对Redis驱动程序异常，将异常转换为Spring的可移植数据访问异常层次结构
* RedisTemplate，提供高级抽象，用于执行各种Redis操作，异常转换和序列化支持
* Pubsub（发布订阅）支持（如 用于消息驱动的POJO的MessageListenerContainer）
* 支持Redis哨兵和集群
* JDK，String，JSON和Spring Object/XML映射序列化
* 在Redis之上的JDK Collection实现
* 原子计数器支持 类
* 排序和管道功能
* 专门支持SORT，SORT / GET模式和返回的批量值
* Redis实现了Spring 3.1缓存抽象
* 自动实现Repository接口，包括使用@EnableRedisRepositories支持自定义查找程序方法
* CDI对存储库的支持

Springboot默认的连接工厂是`RedisConnectionFactory`，它是基于jedis redis库的`JedisconnectionFactory`实例。**连接工厂被注入到消息侦听器容器和Redis模板中**。

在配置文件`application.yml`中，使用`redis-prefix`来配置redis对应数据类型的前缀。


#### 跳板机
`com.self.ry.config`包中是跳板机的设置。
跳板机就是一台服务器，维护人员在维护过程中，首先要统一登录到这台服务器上，然后从这台服务器再登录到目标设备进行维护。
即 跳板机相当于一个内部局域网的外部网关，所有的操作必须通过网关，来操作内部局域网。

<pre>
# redis跳板机设置
ssh-redis:
  # 是否开启 ssh 服务
  enable: true
#  enable: false
  # 跳板机IP地址
  remote: XXX.XXX.XXX.XXX
  port: 14022
  username: root
  password: 2019111
  # 设定安全级别  no  ask   yes
  # 特别注意 此处一定要加''单引号，否则解析成 no-false  yes-true
  strictKey: 'no'
  # 真实的redis的host和port
  redisHost: 192.168.5.140
  redisPort: 6379
</pre>
在此例子中，跳板机是`XXX.XXX.XXX.XXX`，它的端口`14022`对应内部局域网中的主机`192.168.5.140`。这样通过端口-主机映射的方式，
达到操作局域网内部主机的目的。

#### Docker
在`src/main/docker`文件夹中，放置使用docker运行项目的文件，包括使用jenkins构建镜像的脚本文件。