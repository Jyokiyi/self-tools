package com.self.ry.config.redis;

/**
 * Copyright: Copyright (c) 2019
 *
 * @Description: 定义 redis key
 * @Version: v1.0.0
 * @Author ruyi
 * @Date 2019/6/28 12:42
 */
public final class RedisConst {
    // 设备ID-IP的对应
    public static final String IP_ICCID_INFO      = "ip_iccid_info";
}
