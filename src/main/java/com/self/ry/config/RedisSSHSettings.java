package com.self.ry.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Copyright: Copyright (c) 2019
 *
 * @Description: Redis的SSH支持，跳板机属性设置
 * @version: v1.0.0
 * @author: my
 * @date: 2019/1/14 17:23
 * <p>
 * Modification History:
 * Date         Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/1/14     汝毅           v1.0.0               修改原因
 */

@Component
@ConfigurationProperties(prefix = "ssh-redis")
public class RedisSSHSettings {
    /**
     * 是否开启SSH
     */
    private boolean enable;
    /**
     * 跳板机的IP
     */
    private String remote;
    /**
     * 跳板机的端口
     */
    private int port;
    /**
     * 跳板机的登录用户名
     */
    private String username;
    /**
     * 跳板机的登录密码
     */
    private String password;
    /**
     * 安全检查级别
     * 1. no  最不安全的级别，相对安全的内网测试时建议使用。
     *        如果连接server的key在本地不存在，那么就自动添加到文件中（默认是known_hosts），并且给出一个警告。
     * 2. ask 默认的级别,如果连接和key不匹配，给出提示，并拒绝登录
     * 3. yes 最安全的级别，如果连接与key不匹配，就拒绝连接，不会提示详细信息
     */
    private String strictKey;
    /**
     * 真实的redis主机host
     */
    private String redisHost;
    /**
     * 真实的redis主机暴露的端口
     */
    private int redisPort;

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getRemote() {
        return remote;
    }

    public void setRemote(String remote) {
        this.remote = remote;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStrictKey() {
        return strictKey;
    }

    public void setStrictKey(String strictKey) {
        this.strictKey = strictKey;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }
}
