package com.self.ry.controller2;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright: Copyright (c) 2019
 *
 * @Description: 该类的功能描述
 * @Version: v1.0.0
 * @Author ruyi
 * @Date 2019/6/27 16:49
 */
@RestController
@RequestMapping("/test2")
@Api(tags = {"测试2"})
public class TestController2 {


    @GetMapping("first")
    @ApiOperation("获取一个信息2")
    public Map<String, Object> first() {
        Map<String, Object> result = new HashMap<>();
        result.put("name", "ruyi");
        return result;
    }
}
