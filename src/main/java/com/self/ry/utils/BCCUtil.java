package com.self.ry.utils;

/**
 * @Description: BCC(Block Check Character/信息组校验码)
 * 校验码是将所有数据异或得出，俗称异或校验
 * 具体算法是：将每一个字节的数据（一般是两个16进制的字符）进行异或后即得到校验码。
 * @Version: v1.0.0
 * @Author ruyi
 * @Date 2019/6/5 11:22
*/
public final class BCCUtil {

    public static int BCC(byte[] src) {
        byte bcc = src[0];
        for (int i = 1, len = src.length; i < len; i++) {
            bcc ^= src[i];
        }

        return bcc & 0xFF;
    }
}
