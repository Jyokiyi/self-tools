package com.self.ry.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Copyright: Copyright (c) 2019
 *
 * @Description: 该类的功能描述
 * @version: v1.0.0
 * @author: my
 * @date: 2019/2/19 17:24
 * <p>
 * Modification History:
 * Date         Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/2/19     my           v1.0.0               修改原因
 */
public class StringUtils {
    /**
     * <p>Checks if a CharSequence is empty (""), null or whitespace only.</p>
     *
     * <p>Whitespace is defined by {@link Character#isWhitespace(char)}.</p>
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param cs  the CharSequence to check, may be null
     * @return {@code true} if the CharSequence is null, empty or whitespace only
     * @since 2.0
     * @since 3.0 Changed signature from isBlank(String) to isBlank(CharSequence)
     */
    public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>Checks if a CharSequence is not empty (""), not null and not whitespace only.</p>
     *
     * <p>Whitespace is defined by {@link Character#isWhitespace(char)}.</p>
     *
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param cs  the CharSequence to check, may be null
     * @return {@code true} if the CharSequence is
     *  not empty and not null and not whitespace only
     * @since 2.0
     * @since 3.0 Changed signature from isNotBlank(String) to isNotBlank(CharSequence)
     */
    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * <p>Checks if a CharSequence is empty ("") or null.</p>
     *
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty("")        = true
     * StringUtils.isEmpty(" ")       = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ") = false
     * </pre>
     *
     * <p>NOTE: This method changed in Lang version 2.0.
     * It no longer trims the CharSequence.
     * That functionality is available in isBlank().</p>
     *
     * @param cs  the CharSequence to check, may be null
     * @return {@code true} if the CharSequence is empty or null
     * @since 3.0 Changed signature from isEmpty(String) to isEmpty(CharSequence)
     */
    public static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }


    /**
     * 逆转 字节
     * @param src 16进制数据
     * @return
     */
    public static String reserveString(String src) {
        int mLen = src.length();
        List<String> list = new ArrayList<>(mLen / 2);
        for (int m = 0; m < mLen; ) {
            list.add(src.substring(m, m + 2));
            m = m + 2;
        }

        Collections.reverse(list);
        return list.stream().collect(Collectors.joining());
    }

    /**
     * 替换字符串中的Unicode
     * @param source 源字符串
     * @param replace 替换字符串
     * @return 替换后的字符串
     */
    public static String replaceWrongUnicode(String source, String replace) {
        if (StringUtils.isBlank(source)) {
            return source;
        }
        if (StringUtils.isBlank(replace)) {
            replace = "";
        }
        Pattern CRLF = Pattern.compile("([\\u007f-\\u009f]|\\u00ad|[\\u0483-\\u0489]|[\\u0559-\\u055a]|\\u058a|[\\u0591-\\u05bd]|\\u05bf|[\\u05c1-\\u05c2]|[\\u05c4-\\u05c7]|[\\u0606-\\u060a]|[\\u063b-\\u063f]|\\u0674|[\\u06e5-\\u06e6]|\\u070f|[\\u076e-\\u077f]|\\u0a51|\\u0a75|\\u0b44|[\\u0b62-\\u0b63]|[\\u0c62-\\u0c63]|[\\u0ce2-\\u0ce3]|[\\u0d62-\\u0d63]|\\u135f|[\\u200b-\\u200f]|[\\u2028-\\u202e]|\\u2044|\\u2071|[\\uf701-\\uf70e]|[\\uf710-\\uf71a]|\\ufb1e|[\\ufc5e-\\ufc62]|\\ufeff|\\ufffc)");
        Matcher m = CRLF.matcher(source);
        if (m.find()) {
            return m.replaceAll(replace);
        }
        return source;
    }


    /**
     * 填充字符操作
     * @param src 源字符串
     * @param fillStr 指定的填充字符
     * @param length 总长度
     * @param position 0-前置填充   1-后置填充
     * @return
     */
    public static String strFill(String src, CharSequence fillStr, int length, int position){
        int length1 = src.length();
        // 若当前字符串长度 大于等于指定的长度 返回源字符串
        if (length1 >= length) return src;

        StringBuffer buffer = new StringBuffer();
        // 后置补0
        if (position == 1) {
            buffer.append(src);
        }

        for (int i = 0; i < length - length1; i++){
            buffer.append(fillStr);
        }

        // 前置补0
        if (position == 0) {
            buffer.append(src);
        }

        return buffer.toString();
    }

    /**
     * 在src前/后 填充指定的字符
     * @param src 源字符串
     * @param length 总长度
     * @param position 0-前置填充   1-后置填充
     * @return
     */
    public static String strFillWithZero(String src, int length, int position){
        return strFill(src, "0", length, position);
    }

    /**
     * 在src前置填充指定的字符
     * @param src 源字符串
     * @param fillStr 指定的填充字符
     * @param length 总长度
     * @return
     */
    public static String strFillPrefix(String src, CharSequence fillStr, int length){
        return strFill(src, fillStr, length, 0);
    }

    /**
     * 在src后置填充指定的字符
     * @param src 源字符串
     * @param fillStr 指定的填充字符
     * @param length 总长度
     * @return
     */
    public static String strFillSuffix(String src, CharSequence fillStr, int length){
        return strFill(src, fillStr, length, 1);
    }
}
