package com.self.ry.utils.lambda;

import java.util.stream.Collector;

public interface Tuple {
	static <T1, T2> Tuple2<T1, T2> tuple(T1 v1, T2 v2) {
		return new Tuple2<>(v1, v2);
	}

	static <T, A1, A2, D1, D2> Collector<T, Tuple2<A1, A2>, Tuple2<D1, D2>> collectors(Collector<T, A1, D1> collector1,
                                                                                       Collector<T, A2, D2> collector2) {
		return Collector.of(
				() -> tuple(collector1.supplier().get(), collector2.supplier().get()), 
				(a, t) -> {
					collector1.accumulator().accept(a.v1, t);
					collector2.accumulator().accept(a.v2, t);
				}, 
				(a1, a2) -> tuple(collector1.combiner().apply(a1.v1, a2.v1), collector2.combiner().apply(a1.v2, a2.v2)),
				
				a -> tuple(collector1.finisher().apply(a.v1), collector2.finisher().apply(a.v2)));
	}
	
}
