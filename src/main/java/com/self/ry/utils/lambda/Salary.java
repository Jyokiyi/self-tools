package com.self.ry.utils.lambda;

import java.util.Random;

public class Salary {
	private String name;
	private double totalSalary;
	private long number;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getTotalSalary() {
		return totalSalary;
	}
	public void setTotalSalary(double totalSalary) {
		this.totalSalary = totalSalary;
	}
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; ++i) {
            int number = random.nextInt(26);// [0,25)
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
	
	@Override
    public String toString() {
        return "{ name:" + name+", totalSalary:"+totalSalary+",number:"+number+"}";
    }   
}
