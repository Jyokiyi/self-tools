package com.self.ry.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Copyright: Copyright (c) 2019
 *
 * @Description: 十进制数字格式
 * 包含货币
 * @version: v1.0.0
 * @author: ruyi
 * @date: 2019/7/14 14:53
 * <p>
 * Modification History:
 * Date         Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/7/14     ruyi           v1.0.0               修改原因
 */
public final class DecimalFormatUtil {


    public static void main(String[] args) {
        DecimalFormat format2 = new DecimalFormat("##0.##E0");
        System.out.println(format2.format(12345));

        List<String> patterns = Arrays.asList(
                "0.00",
                ".##%", // 数值乘100后格式化输出
                "#,###.##",
                "##0.##E0"
        );
        List<Object> params = new ArrayList<Object>() {{
            add(123456); // 四舍五入
            add(-123456f); // 自动使用负子模式
            add(10100f); // #位对应0，且之后无有效数,则省略0
            add(12345678f); // 指数是3的倍数
            add(0.0012345f); // 调整指数实现最小整数位数
        }};
        patterns.forEach(a -> {
            DecimalFormat format = new DecimalFormat(a);
            params.forEach(param -> {
                System.out.print(format.format(param) + ";");
            });
            System.out.println();
        });
    }

    private static void test1() {
        Locale[] locales = NumberFormat.getAvailableLocales();
        double myNumber = -1234.56;
        NumberFormat form;
        for (int j = 0; j < 4; ++j) {
            System.out.println("FORMAT");
            for (int i = 0; i < locales.length; ++i) {
                if (locales[i].getCountry().length() == 0) {
                    continue; // Skip language-only locales
                }
                System.out.print(locales[i].getDisplayName());
                switch (j) {
                    case 0:
                        form = NumberFormat.getInstance(locales[i]);
                        break;
                    case 1:
                        form = NumberFormat.getIntegerInstance(locales[i]);
                        break;
                    case 2:
                        form = NumberFormat.getCurrencyInstance(locales[i]);
                        break;
                    default:
                        form = NumberFormat.getPercentInstance(locales[i]);
                        break;
                }
                if (form instanceof DecimalFormat) {
                    System.out.print(": " + ((DecimalFormat) form).toPattern());
                }
                System.out.print(" -> " + form.format(myNumber));
                try {
                    System.out.println(" -> " + form.parse(form.format(myNumber)));
                } catch (ParseException e) {
                }
            }
        }
    }
}
