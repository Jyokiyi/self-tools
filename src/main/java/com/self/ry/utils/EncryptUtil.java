package com.self.ry.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Description: 用于 用户密码的加密
 * @Version: v1.0.0
 * @Author ruyi
 * @Date 2019/6/5 11:21
*/
public final class EncryptUtil {

    /**
     * 传入文本内容，返回 SHA-256 串
     *
     * @param strText
     * @return
     */
    public static String SHA256(final String strText) {
        return SHA(strText, "SHA-256");
    }

    /**
     * 传入文本内容，返回 SHA-512 串
     *
     * @param strText
     * @return
     */
    public static String SHA512(final String strText) {
        return SHA(strText, "SHA-512");
    }

    /**
     * 字符串 SHA 加密
     *
     * @param strText
     * @param strType
     * @return
     */
    private static String SHA(final String strText, final String strType) {
        // 返回值
        String strResult = null;

        // 是否是有效字符串
        if (strText != null && strText.length() > 0) {
            try {
                // SHA 加密开始
                // 创建加密对象 传入加密类型
                MessageDigest messageDigest = MessageDigest.getInstance(strType);
                // 传入要加密的字符串
                messageDigest.update(strText.getBytes());
                // 得到 byte 类型结果
                byte byteBuffer[] = messageDigest.digest();

                // 将字节转换为String
                StringBuffer strHexString = new StringBuffer();
                // 遍历 byte buffer
                for (int i = 0; i < byteBuffer.length; i++) {
                    String hex = Integer.toHexString(0xff & byteBuffer[i]);
                    if (hex.length() == 1) {
                        strHexString.append('0');
                    }
                    strHexString.append(hex);
                }
                // 得到返回结果
                strResult = strHexString.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return strResult;
    }

    /**
     * 密码加密
     *
     * 获取加密后密码
     * 随机加盐
     * @param password 密码 原始值
     * @return 加密后的数据
     */
    public static String getSecurityPwd(String password) {
        // BCrypt.gensalt()工作因子 默认是10，可以BCrypt.gensalt(12)指定长度
        return BCrypt.hashpw(EncryptUtil.SHA512(password), BCrypt.gensalt());
    }

    /**
     * 检查密码
     *
     * @param pwd
     * @param sault
     * @return true:密码一致，false：密码不一致
     */
    public static boolean checkPwd(String pwd, String sault) {
        return BCrypt.checkpw(EncryptUtil.SHA512(pwd), sault);
    }


    public static void main(String[] args) {
        String password = "123456!";
        System.out.println(password.length());
        String gensalt = BCrypt.gensalt();
        System.out.println(gensalt);
        String hashpw = BCrypt.hashpw(EncryptUtil.SHA512(password), gensalt);
        System.out.println(hashpw);
        System.out.println(hashpw.length());
    }
}
