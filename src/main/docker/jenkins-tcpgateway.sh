#!/bin/bash

rm -f /opt/test/Dockerfile

rm -f /opt/test/ry-tools-1.0.jar

cp -ri /root/.jenkins/workspace/test/target/ry-tools-1.0.jar /opt/test/
cp -ri /root/.jenkins/workspace/test/src/main/docker/Dockerfile /opt/test/

#cd /opt/test/

# 启动jar包
#nohup  java -jar -Xms4096m -Xmx4096m ry-tools-1.0.jar --spring.profiles.active=test > test.log &
# 构建镜像


# 获取container
containerid=`docker ps -a|grep -i ruyi-test|awk '{print $1}'`

# 删除容器
if [ -n "$containerid" ];then
   docker stop $containerid
   docker container rm -f $containerid
   echo "container stop and remove $containerid"
else
   echo $containerid "is null"
fi

# 获取镜像
imagesid=`docker images|grep -i ruyi-test|awk '{print $3}'`

if [ -n "$imagesid" ];then
   docker image rm -f $imagesid
   echo "image remove $imagesid"
else
   echo $imagesid "is null" 
fi

project=/opt/test/

cd $project

# 构建容器
sudo docker build -t ruyi-test:latest .


# 启动docker容器
#sudo docker run -it -d -v /usr/local/jdk/jdk1.8.0_161/:/share/jdk/ -p 8182:8181 --name testtcp ruyi-test
sudo docker run -it -d -v /opt/test/log/:/app/log/ -v /etc/localtime:/etc/localtime -p 8089:8089 ruyi-test --spring.profiles.active=test


echo "Execute shell Finish"